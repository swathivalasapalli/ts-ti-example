import allEven from './allEven';
test('allEven', () => {
  expect(allEven([2, 3, 4])).toEqual([2, 4]);
});
