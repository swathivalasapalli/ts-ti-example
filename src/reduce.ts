export default function reduce(
  f: (x: number, y: number) => number,
  init: number,
  arr: number[]
): number {
  let result = init;
  for (const e of arr) {
    result = f(result, e);
  }
  return result;
}
