import { map, isEven } from './map';
test('isEven', () => {
  expect(isEven(6)).toBeTruthy();
  expect(isEven(8)).toBeTruthy();
});
test('map', () => {
  expect(map(x => x + 1, [1, 2, 3])).toEqual([2, 3, 4]);
});
