export default function filter(
  f: (x: number) => boolean,
  arr: number[]
): number[] {
  const r: number[] = [];
  for (const e of arr) {
    if (f(e)) {
      r.push(e);
    }
  }
  return r;
}
