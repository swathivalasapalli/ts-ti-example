export default function map(f: (x: number) => number, arr: number[]): number[] {
  const r: number[] = [];
  for (const e of arr) {
    r.push(f(e));
  }
  return r;
}
