import all from './all';
import isEven from './isEven';
test('all', () => {
  expect(all(isEven, [1, 7, 3, 5])).toEqual(false);
  expect(all(isEven, [2, 4, 6])).toEqual(true);
  expect(all(isEven, [1, 3, 2, 6, 5])).toEqual(false);
});
