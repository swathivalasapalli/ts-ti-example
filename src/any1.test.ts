import any from './any1';
import isEven from './isEven';
test('any', () => {
  expect(any(isEven, [1, 7, 3, 5])).toEqual(false);
  expect(any(isEven, [2, 3, 5])).toEqual(true);
  expect(any(isEven, [2, 4, 6])).toEqual(true);
  expect(any(isEven, [2, 4, 5])).toEqual(true);
});
