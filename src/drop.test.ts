import drop from './drop';
test('drop', () => {
  expect(drop(2, [1, 2, 3])).toEqual([3]);
  expect(drop(3, [1, 2, 3])).toEqual([]);
  expect(drop(0, [1, 2, 3])).toEqual([1, 2, 3]);
});
