export default function any(f: (x: number) => boolean, arr: number[]): boolean {
  let r = false;
  for (const e of arr) {
    r = r || f(e);
  }
  return r;
}
