export function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
export function any(f: (x: number) => boolean, arr: number[]): boolean {
  let r = false;
  for (const e of arr) {
    r = r || f(e);
    if (!r) {
      return true;
    }
  }
  return r;
}
