import even from './even';
import dropwhile from './takewhile';
test('dropwhile', () => {
  expect(dropwhile(even, [1, 2, 4, 6, 7, 9])).toEqual([1]);
  expect(dropwhile(even, [2, 6, 8, 10])).toEqual([]);
  expect(dropwhile(even, [2, 4, 5, 7, 9])).toEqual([5, 7, 9]);
});
