import isEven from './even';
test('isEven', () => {
  expect(isEven(2)).toBeTruthy();
  expect(isEven(3)).toBeFalsy();
});
