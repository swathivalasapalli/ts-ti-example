export default function every<T, T1>(f: (x: T1) => T1, arr: T[]): T1 {
  let r: T1 = true;
  for (const e of arr) {
    r = r && f(e);
    return r;
  }
}
