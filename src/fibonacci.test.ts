import fibonacci from './fibonacci';
test('fibonacci' , () => {
  expect(fibonacci(6)).toEqual([1, 1, 2, 3 , 5 , 8]);
  expect(fibonacci(5)).toEqual([1, 1, 2, 3 , 5 ]);
)};
