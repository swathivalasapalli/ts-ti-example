export default function allEvenI(arr: number[]): number[] {
  const r: number[] = [];
  for (const e of arr) {
    if (e % 2 === 0) {
      r.push(e);
    }
  }
  return r;
}
// import filterg from './filterg';
// export default function allEven(arr: number[]): number[]{
//       return filterg(e=>e%2===0,arr);
//   }
// }
