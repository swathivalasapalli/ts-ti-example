export function map<T1, T2>(f: (x: T1) => T2, arr: T1[]): T2[] {
  const r: T2[] = [];
  for (const e of arr) {
    r.push(f(e));
  }
  return r;
}
export function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
export function concat2<T>(arr1: T[], arr2: T[]): T[] {
  for (const e of arr2) {
    arr1.push(e);
  }
  return arr1;
}
