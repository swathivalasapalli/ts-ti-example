export default function squareAll(arr: number[]): number[] {
  const r: number[] = [];
  for (const e of arr) {
    r.push(e * e);
  }
  return r;
}
