import sum from './sum';
test('sum', () => {
  expect(sum([1, 2, 3])).toEqual(6);
  expect(sum([1, 2, 5])).toEqual(8);
  expect(sum([0, 2])).toEqual(2);
});
