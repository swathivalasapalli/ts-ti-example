export default function take(n: number, arr: number[]): number[] {
  const a = [];
  for (let i = 0; i < n; i++) {
    a.push(arr[i]);
  }
  return a;
}
