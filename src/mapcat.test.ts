import { concat2, concat, map, inc, mapcat} from './mapcat';
test('concat2', () => {
  expect(concat2([1,2,3,4],[6,7,8])).toEqual([1, 2, 3, 4,6,7,8]);
});

test('concat', () => {
  expect(concat([[1,2,3,4],[6,7,8]])).toEqual([1, 2, 3, 4,6,7,8]);
});
 test('map', () => {
   expect(map(inc, [1, 2, 3])).toEqual([2, 3, 4]);
 });
 test('inc', () => {
expect(inc(1)).toEqual(2);
 });
 test('mapcat', () => {
   expect(mapcat(map,[[1,2, 3],[4, 5, 6]])).toEqual([1, 2, 3, 4, 5, 6]);
 });