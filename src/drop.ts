export default function drop(n: number, arr: number[]): number[] {
  const a = [];
  for (let i = n; i < arr.length; i++) {
    a.push(arr[i]);
  }
  return a;
}
