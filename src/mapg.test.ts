import mapg from './mapg';
test('mapg', () => {
  expect(mapg(x => x * x, [1, 2])).toEqual([1, 4]);
  expect(mapg(x => x + 1, [1, 2])).toEqual([2, 3]);
});
