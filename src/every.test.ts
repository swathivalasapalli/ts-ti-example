import every from './every';
import isEven from './isEven';
test('every', () => {
  expect(every(isEven, [1, 7, 3, 5])).toEqual(false);
  expect(every(isEven, [2, 4, 6])).toEqual(true);
  expect(every(isEven, [1, 3, 2, 6, 5])).toEqual(false);
});
