import concat2 from './map';
export function mapcat<T1, T2>(f: (x: T1) => T2[], ar: T1[]): T2[] {
  let r: T2[] = [];
  for (const e of ar) {
    r = concat2(r, e);
  }
  return r;
}
