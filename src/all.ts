export default function all(f: (x: number) => boolean, arr: number[]): boolean {
  let r = true;
  for (const e of arr) {
    r = r && f(e);
  }
  return r;
}
