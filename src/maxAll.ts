export default function maxAll(arr: number[]): number {
  let m = 0;
  for (const i of arr) {
    if (arr[i] > m) {
      m = arr[i];
    }
  }
  return m;
}
