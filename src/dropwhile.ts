export default function dropwhile(f:(x:T)=>T[],arr:T[])=> T[] {
  for (let i = 0; i < arr.length; i++){
    if (f(arr[i])) {
      if (!f(arr[i + 1])) {
        return arr.slice(i + 1);
      }
      else {
        return arr;
      }
    }
    }
};
