
export {concat2, concat, map, inc, mapcat};
const concat2=( arr1 : number[], arr2 : number[]):number[] {
    for(const e of arr2){
    arr1.push(e);
  }
  return arr1;
}

 const concat=(arr:number[][]):number[] {
let result:number[]=[];
for(const e of arr)
{
result=concat2(result,e);
}
return result;
 }
 const  map = (f:(x: number) => number, arr: number[]):number[]  {
  const r:number[] =[];
  for(const e of arr)
  {
      r.push( f(e));
  }
  return r;
}
const inc=(n: number):number {
  return n+=1;
}

const mapcat =(f:(x: number) => number[], ar:number[]): number[] {
  let r: number[]=[];
  for(const e of ar){
    r=concat2(r, e);
  }
  return r;
}