import filterg from './filterg';
test('filterg', () => {
  expect(filterg(x => x % 2 === 0, [1, 2, 3, 4])).toEqual([2, 4]);
});
