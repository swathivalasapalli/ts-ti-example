export default function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
