import stack from './stack';
test('stack', () => {
  const s = new stack();
  expect(s.push(1)).toEqual(1);
  expect(s.push(2)).toEqual(2);
  expect(s.push(3)).toEqual(3);
  expect(s.pop()).toEqual(3);
  expect(s.pop()).toEqual(2);
  expect(s.pop()).toEqual(1);
  expect(s.isEmpty()).toBeTruthy();
  expect(s.size()).toEqual(0);
});
