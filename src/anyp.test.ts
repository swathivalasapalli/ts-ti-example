import { any, isEven } from './anyp';

test('any', () => {
  expect(any(isEven, [1, 6, 3])).toEqual(true);
  expect(any(isEven, [2, 4, 6])).toEqual(true);
  expect(any(isEven, [1, 3, 2, 6, 5])).toEqual(true);
  expect(any(isEven, [2, 4, 6, 8])).toEqual(true);
  expect(any(isEven, [1, 3, 5])).toEqual(false);
});
