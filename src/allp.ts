export function all(f: (x: number) => boolean, arr: number[]): boolean {
  let r = true;
  for (const e of arr) {
    r = r && f(e);
    if (!r) {
      return false;
    }
  }
  return r;
}

export function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
