export default function sum(arr: number[]): number {
  let s = 0;
  for (const e of arr) {
    s += e;
  }
  return s;
}
