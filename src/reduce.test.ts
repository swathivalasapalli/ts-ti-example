import add from './add';
import reduce from './reduce';
test('reduce', () => {
  expect(reduce(add, 1, [1, 2])).toEqual(4);
});
