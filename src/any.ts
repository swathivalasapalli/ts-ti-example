export default function any<T, T1>(f: (x: T1) => T1, arr: T[]): T1 {
  let r: T1 = false;
  for (const e of arr) {
    r = r || f(e);
    return r;
  }
}
