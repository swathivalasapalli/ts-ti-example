import squareAll from './squareAll';
test('squareAll', () => {
  expect(squareAll([1, 2])).toEqual([1, 4]);
  expect(squareAll([1])).toEqual([1]);
  expect(squareAll([1, 2, 3])).toEqual([1, 4, 9]);
  expect(squareAll([0, 2])).toEqual([0, 4]);
});
